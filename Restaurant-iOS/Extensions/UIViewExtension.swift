//
//  UIViewExtension.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 18.11.2020.
//

import UIKit

extension UIView {
    
    func setCustomBG(bgColor: UIColor?, cornerRadius: CGFloat = 4) {
        layer.cornerRadius = cornerRadius
        backgroundColor = bgColor
    }
    
    func setCustomBorder(color: UIColor?, width: CGFloat = 1, cornerRadius: CGFloat = 4) {
        layer.borderWidth = width
        layer.borderColor = color?.cgColor
        layer.cornerRadius = cornerRadius
    }
}
