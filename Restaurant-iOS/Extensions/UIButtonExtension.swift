//
//  UIButtonExtension.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//


import UIKit

extension UIButton {
    
    // MARK: - Set Images
    func setImages(left: UIImage? = nil, right: UIImage? = nil, titleInset: CGFloat, imageSideInset: CGFloat, titleAlign: UIControl.ContentHorizontalAlignment = .left) {
        
        // imageSideInset - Left or right inset
        // titleInset - Title inset from images
        let width: CGFloat = 24
        let height: CGFloat = 24
        
        // Only left image
        if let leftImage = left, right == nil {
            setImage(leftImage, for: .normal)
            imageEdgeInsets = UIEdgeInsets(top: 0, left: imageSideInset, bottom: 0, right: 0)
            titleEdgeInsets = UIEdgeInsets(top: 0,
                                           left: imageEdgeInsets.left + titleInset,
                                           bottom: 0,
                                           right: imageSideInset)
            contentHorizontalAlignment = titleAlign
        }
        
        // Only right image
        if let rightImage = right, left == nil {
            setImage(rightImage, for: .normal)
            
            imageEdgeInsets = UIEdgeInsets(top: 0,
                                           left: bounds.width - width - imageSideInset,
                                           bottom: 0,
                                           right: 0)
            titleEdgeInsets = UIEdgeInsets(top: 0,
                                           left: imageSideInset - width,
                                           bottom: 0,
                                           right: imageSideInset + width + titleInset)
            contentHorizontalAlignment = titleAlign
        }
        
        // Left and right image
        if let leftImage = left, let rightImage = right {
            setImage(leftImage, for: .normal)
            imageEdgeInsets = UIEdgeInsets(top: 0, left: imageSideInset, bottom: 0, right: 0)
            
            let rightImageView = UIImageView(frame: CGRect(
                                                x: bounds.maxX - (width + imageSideInset),
                                                y: bounds.midY - (height / 2),
                                                width: width,
                                                height: height))
            
            rightImageView.image = rightImage
            rightImageView.contentMode = .scaleAspectFit
            rightImageView.layer.masksToBounds = true
            addSubview(rightImageView)
            
            contentHorizontalAlignment = titleAlign
            titleEdgeInsets = UIEdgeInsets(top: 0,
                                           left: imageEdgeInsets.left + titleInset,
                                           bottom: 0,
                                           right: rightImageView.frame.width + imageSideInset + titleInset)
        }
    }
    
    
    func setCustomTitle(title: String, titleColor: UIColor?, fontSize: CGFloat, fontWeight: UIFont.Weight) {
        setTitle(title, for: .normal)
        setTitleColor(titleColor, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
    }
    
}
