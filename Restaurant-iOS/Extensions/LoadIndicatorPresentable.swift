//
//  LoadIndicatorPresentable.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 05.12.2020.
//

import UIKit

protocol LoadIndicatorPresentable: class {
    func showLoadIndicator()
    func showTransparentLoadIndicator()
    func hideLoadIndicator()
}


extension UIViewController: LoadIndicatorPresentable {
    
    func showLoadIndicator() {
        showLoadIndicator(bgColor: .white)
    }
    
    func showTransparentLoadIndicator() {
        showLoadIndicator(bgColor: UIColor.RestColors.gray)
    }
    
    private func showLoadIndicator(bgColor: UIColor) {
        let loadingView = LoadIndicatorView.assemblyWith(frame: view.bounds)
        loadingView.backgroundColor = bgColor
        view.addSubview(loadingView)
    }
    
    func hideLoadIndicator() {
        view.subviews.forEach { (subView) in
            if subView.isKind(of: LoadIndicatorView.self) {
                subView.removeFromSuperview()
            }
        }
    }
}
