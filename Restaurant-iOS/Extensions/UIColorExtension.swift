//
//  UIColorExtension.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import UIKit

extension UIColor {
    enum RestColors  {
        static var lightGray: UIColor { return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.06) } // 6
        static var gray: UIColor { return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.64)} // 64
        static var blue: UIColor { return #colorLiteral(red: 0, green: 0.5058823529, blue: 0.8705882353, alpha: 1) }
        static var red: UIColor { return #colorLiteral(red: 0.8705882353, green: 0, blue: 0, alpha: 1) }
        static var orange: UIColor { return #colorLiteral(red: 0.9529411765, green: 0.4901960784, blue: 0.05882352941, alpha: 1)}
    }
}
