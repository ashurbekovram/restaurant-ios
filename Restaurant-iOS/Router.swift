//
//  Router.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 04.12.2020.
//

import UIKit

protocol RouterProtocol {
    var navigationController: UINavigationController { get set }
    var screenAssembler: ScreenAssemblerProtocol { get set }
    
    func initialViewController()
    func popToRoot()
    func pushMenuVC()
    func pushProductDetailVC()
    func pushCartVC()
    func pushTableReservationVC()
    func pushAddUserDataVC()
    func pushDeliveryVC()
    func pushThanksVC()
}


class Router: RouterProtocol {
    
    var navigationController: UINavigationController
    var screenAssembler: ScreenAssemblerProtocol
    
    init(navigationController: UINavigationController, screenAssembler: ScreenAssemblerProtocol) {
        self.navigationController = navigationController
        self.screenAssembler = screenAssembler
    }
    
    func initialViewController() {
        let mainVC = screenAssembler.assembleMainScreen(router: self)
        navigationController.viewControllers = [mainVC]
    }
    
    func popToRoot() {
        navigationController.popToRootViewController(animated: true)
    }
    
    func pushMenuVC() {
        let menuVC = screenAssembler.assembleMenuScreen(router: self)
        navigationController.pushViewController(menuVC, animated: true)
    }
    
    func pushProductDetailVC() {
        let productDetailVC = screenAssembler.assembleProductDetailScreen(router: self)
        navigationController.pushViewController(productDetailVC, animated: true)
    }
    
    func pushCartVC() {
        let cartVC = screenAssembler.assembleCartScreen(router: self)
        navigationController.pushViewController(cartVC, animated: true)
    }
    
    func pushTableReservationVC() {
        let tableReservationVC = screenAssembler.assembleTableReservationScreen(router: self)
        navigationController.pushViewController(tableReservationVC, animated: true)
    }
    
    func pushAddUserDataVC() {
        let addUserDataVC = screenAssembler.assembleAddUserDataScreen(router: self)
        navigationController.pushViewController(addUserDataVC, animated: true)
    }
    
    func pushDeliveryVC() {
        let deliveryVC = screenAssembler.assembleDeliveryScreen(router: self)
        navigationController.pushViewController(deliveryVC, animated: true)
    }
    
    func pushThanksVC() {
        let thanksVC = screenAssembler.assembleThanksScreen(router: self)
        navigationController.pushViewController(thanksVC, animated: true)
    }
}
