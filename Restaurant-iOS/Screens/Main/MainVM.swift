//
//  MainVM.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import Foundation

protocol MainVMProtocol {
    func showMenuVC()
    func showTableReservationVC()
}


class MainVM: MainVMProtocol {
    private var router: RouterProtocol
    
    init(router: RouterProtocol) {
        self.router = router
    }
    
    func showMenuVC() {
        UserData.shared.orderType = .delivery
        router.pushMenuVC()
    }
    
    func showTableReservationVC() {
        router.pushTableReservationVC()
    }
}
