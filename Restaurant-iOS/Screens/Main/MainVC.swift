//
//  MainVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import UIKit

class MainVC: UIViewController {
    
    //MARK: - IB Outlets
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var reserveButton: UIButton!
    @IBOutlet weak var scanButton: UIButton!
    
    
    //MARK: - Properties
    public var viewModel: MainVMProtocol!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setDesign()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    // MARK: - Private Func
    private func setDesign() {
        menuButton.setCustomBorder(color: R.color.gray06())
        menuButton.setCustomTitle(title: "Оформить доставку", titleColor: .black, fontSize: 16, fontWeight: .medium)
        menuButton.tintColor = .black
        menuButton.setImages(
            left: R.image.iconRest()?.withRenderingMode(.alwaysOriginal),
            right: R.image.chevronRight(),
            titleInset: 12,
            imageSideInset: 16)
        
        bookButton.setCustomBorder(color: R.color.gray06())
        bookButton.setCustomTitle(title: "Книга жалоб и предложений", titleColor: .black, fontSize: 16, fontWeight: .medium)
        bookButton.tintColor = .black
        bookButton.setImages(
            left: R.image.iconBook()?.withRenderingMode(.alwaysOriginal),
            right: R.image.chevronRight(),
            titleInset: 12,
            imageSideInset: 16)
        
        reserveButton.setCustomBorder(color: R.color.gray06())
        reserveButton.setCustomTitle(title: "Забронировать столик", titleColor: .black, fontSize: 16, fontWeight: .medium)
        reserveButton.tintColor = .black
        reserveButton.setImages(
            left: R.image.iconLock()?.withRenderingMode(.alwaysOriginal),
            right: R.image.chevronRight(),
            titleInset: 12,
            imageSideInset: 16)
        
        scanButton.setTitle(.none, for: .normal)
        scanButton.setImage(R.image.iconScan(), for: .normal)
        scanButton.setCustomBorder(color: R.color.gray06(), width: 1, cornerRadius: scanButton.frame.height / 2)
    }
    
    
    // MARK: - IB Actions
    @IBAction func showMenuAction(_ sender: Any) {
        viewModel.showMenuVC()
    }
    
    @IBAction func showCommentBookAction(_ sender: Any) {
    }
    
    @IBAction func showTableReservationAction(_ sender: Any) {
        viewModel.showTableReservationVC()
    }
    
    @IBAction func showScanAction(_ sender: Any) {
    }
}
