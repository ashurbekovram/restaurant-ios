//
//  SliderCVCell.swift
//  Restaurant-iOS
//
//  Created by Асельдер on 11.12.2020.
//

import UIKit
import SDWebImage

class ImageCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func setData(imageURL: String) {
        imageView.sd_setImage(with: URL(string: imageURL))
    }
    
}
