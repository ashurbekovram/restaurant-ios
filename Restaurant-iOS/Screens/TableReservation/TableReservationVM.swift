//
//  TableReservationVM.swift
//  Restaurant-iOS
//
//  Created by Асельдер on 10.12.2020.
//

import Foundation
import RxSwift

protocol TableReservationVMProtocol {
    var viewState: BehaviorSubject<ViewState> { get set }
    var tables: [Table] { get set }
    
    func startFetch()
    func showAddUserData()
}


class TableReservationVM: TableReservationVMProtocol {
    
    private let router: RouterProtocol
    
    var viewState: BehaviorSubject<ViewState>
    var tables: [Table] = []
    
    
    init(router: RouterProtocol) {
        self.router = router
        self.viewState = .init(value: .initial)
    }
    
    
    func startFetch() {
        self.viewState.onNext(.loading)
        
        Api.v1.getTables().responseData { [weak self] (response) in
            switch response.result {
            case .success(let resultJSON):
                let resultData = try? JSONDecoder().decode([Table].self, from: resultJSON)
                guard let tables = resultData else {
                    print("Ошибка декодирования резервации столиков")
                    return
                }
                self?.tables = tables
                self?.viewState.onNext(.success)
                
            case .failure(let error):
                print(error.localizedDescription)
                self?.viewState.onNext(.failure)
            }
        }
    }
    
    
    func showAddUserData() {
        router.pushAddUserDataVC()
    }
}
