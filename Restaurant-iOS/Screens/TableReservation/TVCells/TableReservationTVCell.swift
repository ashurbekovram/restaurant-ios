//
//  TableReservationTVCell.swift
//  Restaurant-iOS
//
//  Created by Асельдер on 10.12.2020.
//

import UIKit
import SDWebImage

class TableReservationTVCell: UITableViewCell {
    
    @IBOutlet weak var tableCardView: UIView!
    @IBOutlet weak var personCountLabel: UILabel!
    @IBOutlet weak var tableNumberLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var reserveButton: UIButton!
    @IBOutlet weak var labelsSeparatorView: UIView!
    
    
    private var viewModel: TableReservationVMProtocol?
    private var table: Table!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        setDesign()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func goToReservationAction(_ sender: Any) {
        viewModel?.showAddUserData()
    }
    
    
    func setData(table: Table, viewModel: TableReservationVMProtocol?) {
        self.table = table
        self.viewModel = viewModel
        
        personCountLabel.text = String(table.personsCount) + " персон"
        tableNumberLabel.text = "Стол №" + String(table.tableNumber)
        collectionView.reloadData()
    }
    
    
    private func setDesign() {
        tableCardView.setCustomBorder(color: R.color.gray06())
        
        reserveButton.tintColor = R.color.blue()
        reserveButton.setImages(right: R.image.chevronRight(),
                                titleInset: 0,
                                imageSideInset: 0,
                                titleAlign: .right)
        reserveButton.setCustomTitle(title: "Забронировать",
                                     titleColor: R.color.blue(),
                                     fontSize: 13,
                                     fontWeight: .medium)
        
        personCountLabel.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        tableNumberLabel.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        labelsSeparatorView.backgroundColor = R.color.gray06()
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isPagingEnabled = true
    }
    

}



// MARK: - CollectionView DataSource
extension TableReservationTVCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        table.tableImages.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.imageCVCell, for: indexPath)!
        
        item.setData(imageURL: table.tableImages[indexPath.item])
        return item
    }
}



// MARK: - CollectionView Delegate
extension TableReservationTVCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize(width: width, height: height)
    }
}
