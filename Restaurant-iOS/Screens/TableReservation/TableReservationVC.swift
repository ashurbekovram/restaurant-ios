//
//  TableReservationVC.swift
//  Restaurant-iOS
//
//  Created by Асельдер on 10.12.2020.
//

import UIKit
import RxSwift

class TableReservationVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: TableReservationVMProtocol!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subscribeOnViewModel()
        viewModel.startFetch()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 56, right: 0)
        tableView.allowsSelection = false
    }
    
    
    private func subscribeOnViewModel() {
        
        viewModel.viewState.subscribe(onNext: { [ weak self ] (viewState) in
            switch viewState {
            case .initial:
                print("tableReservationVC initial")
            case .loading:
                print("tableReservationVC loading")
                self?.showLoadIndicator()
            case .success:
                print("tableReservationVC success")
                self?.reloadData()
                self?.hideLoadIndicator()
            case .failure:
                print("tableReservationVC failure")
                self?.hideLoadIndicator()
            }
        }).disposed(by: disposeBag)
    }
    
    private func reloadData() {
        tableView.reloadData()
    }
}



extension TableReservationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tables.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.tableReservationTVCell, for: indexPath)!
        
        cell.setData(table: viewModel.tables[indexPath.row], viewModel: viewModel)
        
        return cell
    }
}
