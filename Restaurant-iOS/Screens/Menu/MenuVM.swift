//
//  MenuVM.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import Foundation
import RxSwift

protocol MenuVMProtocol: ProductCellDelegate, CartBottomViewDelegate {
    var viewState: BehaviorSubject<ViewState> { get set }
    var categories: [Category] { get set }
    func startFetch()
}


class MenuVM: MenuVMProtocol {
    
    private var router: RouterProtocol
    var viewState: BehaviorSubject<ViewState>
    var categories: [Category] = []
    
    
    init(router: RouterProtocol) {
        self.router = router
        self.viewState = .init(value: .initial)
    }
    
    
    func startFetch() {
        self.viewState.onNext(.loading)
        
        Api.v1.getMenuData().responseData { [weak self] (response) in
            switch response.result {
            case .success(let resultJSON):
                let resultData = try? JSONDecoder().decode([Category].self, from: resultJSON)
                guard let categories = resultData else {
                    print("Ошибка декодирования меню")
                    return
                }
                self?.categories = categories
                self?.viewState.onNext(.success)
                
            case .failure(let error):
                print(error.localizedDescription)
                self?.viewState.onNext(.failure)
            }
        }
    }
    
    
    // MARK: - ProductCellDelegate
    func showProductDetailVC(id: Int) {
        // todo
        // В рабочей версии должен передаваться id товара
        router.pushProductDetailVC()
    }
    
    
    func addToCart(product: Product) {
        Cart.shared.addToCart(product: product)
    }
    
    
    func decreaceInCart(product: Product) {
        Cart.shared.decreaceInCart(product: product)
    }
    
    
    func deleteFromCart(product: Product) {
        Cart.shared.deleteFromCart(product: product)
    }
    
    
    // MARK: - CartBottomView Delegate
    func showCartVC() {
        router.pushCartVC()
    }
}
