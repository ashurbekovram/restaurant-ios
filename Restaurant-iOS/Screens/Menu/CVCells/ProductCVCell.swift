//
//  ProductCVCell.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import UIKit
import SDWebImage

class ProductCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    
    
    public var delegate: ProductCellDelegate?
    private var product: Product!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDesign()
    }
    
    
    @IBAction func addToCartAction(_ sender: Any) {
        delegate?.addToCart(product: product)
        checkProductInCart()
    }
    
    @IBAction func plusAction(_ sender: Any) {
        delegate?.addToCart(product: product)
        checkProductInCart()
    }
    
    
    @IBAction func minusAction(_ sender: Any) {
        delegate?.decreaceInCart(product: product)
        checkProductInCart()
    }
    
    
    func setData(product: Product, delegate: ProductCellDelegate) {
        self.product = product
        self.delegate = delegate
        
        let imageURL = URL(string: product.image)
        let placeholderImage = UIImage(named: "productPlaceholder")
        imageView.sd_setImage(with: imageURL,
                                   placeholderImage: placeholderImage)
        productNameLabel.text = product.name
        priceLabel.text = String(product.price) + " ₽"
        
        checkProductInCart()
    }
    
    
    private func checkProductInCart() {
        let count = Cart.shared.checkCountOf(product: product)
        countLabel.text = String(count)
        
        if Cart.shared.isAddedToCart(product: product) {
            addToCartButton.isHidden = true
            countView.isHidden = false
        } else {
            addToCartButton.isHidden = false
            countView.isHidden = true
        }
    }
    
    
    private func setDesign() {
        contentView.setCustomBorder(color: R.color.gray06())
        contentView.backgroundColor = .white
        
        imageView.image?.withRenderingMode(.alwaysOriginal)
        
        addToCartButton.setCustomBG(bgColor: UIColor.RestColors.orange)
        addToCartButton.setCustomTitle(title: "Заказать", titleColor: .white, fontSize: 14, fontWeight: .medium)
        
        productNameLabel.textColor = .black
        productNameLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        priceLabel.textColor = .black
        priceLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
        plusButton.setCustomBorder(color: R.color.gray06())
        plusButton.setTitle(.none, for: .normal)
        plusButton.setImage(R.image.iconPlus(), for: .normal)
        plusButton.tintColor = R.color.orange()
        
        minusButton.setCustomBorder(color: R.color.gray06())
        minusButton.setTitle(.none, for: .normal)
        minusButton.setImage(R.image.iconMinus(), for: .normal)
        minusButton.tintColor = .black
        
        countLabel.textColor = .black
        countLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
    }
}
