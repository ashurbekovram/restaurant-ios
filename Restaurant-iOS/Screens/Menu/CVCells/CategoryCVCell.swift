//
//  CategoryCVCell.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import UIKit

class CategoryCVCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    private var category: Category!
    override var isSelected: Bool {
        didSet {
            setSelected(isSelected: isSelected)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDesign()
    }
    
    
    func setData(category: Category) {
        self.category = category
        categoryNameLabel.text = category.categoryName
        
        setSelected(isSelected: isSelected)
    }
    
    
    private func setSelected(isSelected: Bool) {
        if isSelected {
            contentView.setCustomBG(bgColor: UIColor.RestColors.blue)
            categoryNameLabel.textColor = .white
        } else {
            contentView.setCustomBorder(color: R.color.gray06())
            contentView.backgroundColor = .white
            categoryNameLabel.textColor = .black
        }
    }
    
    
    private func setDesign() {
        contentView.setCustomBorder(color: R.color.gray06())
        contentView.backgroundColor = .white
        
        categoryNameLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        categoryNameLabel.textColor = .black
    }
}
