//
//  MenuVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import UIKit
import RxSwift

class MenuVC: UIViewController {
    
    // MARK: - IB Outlets
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var productsCollectionView: UICollectionView!
    @IBOutlet weak var separatorView: UIView!
    
    
    // MARK: - Properties
    public var viewModel: MenuVMProtocol!
    private let disposeBag = DisposeBag()
    private var selectedCategory: Int = 0
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Меню"
        
        subscribeOnViewModel()
        collectionViewsSetup()
        setProductCVCellSize()
        
        viewModel.startFetch()
        
        createCartBottomView(delegate: viewModel)
        
        setDesign()
    }
    
    
    
    // MARK: - Private Funcs
    private func subscribeOnViewModel() {
        viewModel.viewState.subscribe(onNext: { [ weak self ] (viewState) in
            switch viewState {
            case .initial:
                print("menuVC initial")
            case .loading:
                print("menuVC loading")
                self?.showLoadIndicator()
            case .success:
                print("menuVC success")
                self?.reloadData()
                self?.hideLoadIndicator()
            case .failure:
                print("menuVC failure")
                self?.hideLoadIndicator()
            }
        }).disposed(by: disposeBag)
    }
    
    
    private func reloadData() {
        categoriesCollectionView.reloadData()
        productsCollectionView.reloadData()
    }
    
    
    private func setDesign() {
        view.backgroundColor = .white
        categoriesCollectionView.backgroundColor = .clear
        productsCollectionView.backgroundColor = .clear
        separatorView.backgroundColor = UIColor.RestColors.lightGray
    }
    
    
    private func setProductCVCellSize() {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 100, right: 16)
        let width = (self.productsCollectionView.frame.width - layout.minimumInteritemSpacing - layout.sectionInset.left - layout.sectionInset.right) / 2
        layout.itemSize = CGSize(width: width, height: width * 1.8)
        
        productsCollectionView.collectionViewLayout = layout
    }
    
    
    private func collectionViewsSetup() {
        categoriesCollectionView.dataSource = self
        categoriesCollectionView.delegate = self

        productsCollectionView.dataSource = self
        productsCollectionView.delegate = self
    }
}




// MARK: - CollectionView DataSource
extension MenuVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.categoriesCollectionView {
            return viewModel.categories.count
        }
        
        if collectionView == self.productsCollectionView {
            guard viewModel.categories.count != 0 else { return 0 }
            return viewModel.categories[selectedCategory].items?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.categoriesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.categoryCVCell, for: indexPath)!
            
            if !viewModel.categories.isEmpty {
                let category = viewModel.categories[indexPath.item]
                cell.setData(category: category)
            }
            
            if selectedCategory == 0 && indexPath.item == 0 {
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
                cell.isSelected = true
            }
            return cell
        }
        
        if collectionView == self.productsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.productCVCell, for: indexPath)!
            
            if let product = viewModel.categories[selectedCategory].items?[indexPath.item] {
                cell.setData(product: product, delegate: viewModel)
            }
            return cell
        }
        
        return UICollectionViewCell()
    }
    
}



// MARK: - CollectionView Delegate
extension MenuVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.categoriesCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as! CategoryCVCell
            
            selectedCategory = indexPath.item
            cell.isSelected = true
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
            productsCollectionView.reloadData()
            productsCollectionView.setContentOffset(CGPoint.zero, animated: true)
            
        }
        
        if collectionView == self.productsCollectionView {
            guard let _ = collectionView.cellForItem(at: indexPath) as? ProductCVCell else { return }
            
            viewModel.showProductDetailVC(id: 0) // todo
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView == self.categoriesCollectionView {
            if let cell = collectionView.cellForItem(at: indexPath) as? CategoryCVCell {
                cell.isSelected = false
            }
        }
    }
}
