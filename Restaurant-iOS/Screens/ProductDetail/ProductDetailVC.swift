//
//  ProductDetailVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import UIKit
import RxSwift
import SDWebImage

class ProductDetailVC: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var imageSliderView: ProductImageSlider!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriprionLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    
    
    public var viewModel: ProductDetailVMProtocol!
    private let disposeBag = DisposeBag()
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subscribeOnViewModel()
        viewModel.startFetch()
        
        createCartBottomView(delegate: viewModel)
        
        setDesign()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkProductInCart()
    }
    
    
    // MARK: - IB Actions
    @IBAction func addToCartAction(_ sender: Any) {
        viewModel.addToCart()
        checkProductInCart()
    }
    
    
    @IBAction func plusAction(_ sender: Any) {
        viewModel.addToCart()
        checkProductInCart()
    }
    
    
    @IBAction func minusAction(_ sender: Any) {
        viewModel.decreaceInCart()
        checkProductInCart()
    }
    
    
    // MARK: - Private func
    private func subscribeOnViewModel() {
        viewModel.viewState.subscribe(onNext: { (viewState) in
            switch viewState {
            case .initial:
                print("productCardVC initial")
            case .loading:
                print("productCardVC loading")
                self.showLoadIndicator()
            case .success:
                print("productCardVC success")
                self.setProductDetail()
                self.hideLoadIndicator()
            case .failure:
                print("productCardVC failure")
            }
        }).disposed(by: disposeBag)
    }
    
    
    private func setDesign() {
        nameLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        nameLabel.textColor = .black
        
        priceView.setCustomBorder(color: R.color.gray06())
        priceLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        priceLabel.textColor = .black
        
        addToCartButton.setCustomTitle(title: "Добавить в заказ", titleColor: .white, fontSize: 16, fontWeight: .medium)
        addToCartButton.setCustomBG(bgColor: UIColor.RestColors.orange)
        
        infoButton.tintColor = UIColor.RestColors.gray
        infoButton.setTitle("", for: .normal)
        
        plusButton.setCustomBorder(color: R.color.gray06())
        plusButton.setTitle(.none, for: .normal)
        plusButton.setImage(R.image.iconPlus(), for: .normal)
        plusButton.tintColor = R.color.orange()
        
        minusButton.setCustomBorder(color: R.color.gray06())
        minusButton.setTitle(.none, for: .normal)
        minusButton.setImage(R.image.iconMinus(), for: .normal)
        minusButton.tintColor = .black
        
        countLabel.textColor = .black
        countLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
    }
    
    
    private func setProductDetail() {
        imageSliderView.setImages(images: viewModel.productDetail?.image ?? [], delegate: nil)
        nameLabel.text = viewModel.productDetail?.name
        descriprionLabel.text = viewModel.productDetail?.description
        priceLabel.text = String(viewModel.productDetail?.price ?? 0) + " ₽"
        
        self.title = viewModel.productDetail?.name
        
        checkProductInCart()
        
        guard !viewModel.additionalProducts.isEmpty else { return }
        let view = AdditionalProductsView.assemble(title: "С этим часто заказывают",
                                                   viewModel: viewModel)
        stackView.addArrangedSubview(view)
    }
    
    
    private func checkProductInCart() {
        guard let product = viewModel.productDetail?.detailToProduct() else { return }
        
        let count = Cart.shared.checkCountOf(product: product)
        countLabel.text = String(count)
        
        if Cart.shared.isAddedToCart(product: product) {
            addToCartButton.isHidden = true
            countView.isHidden = false
        } else {
            addToCartButton.isHidden = false
            countView.isHidden = true
        }
    }
}
