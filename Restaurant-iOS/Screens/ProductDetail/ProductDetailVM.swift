//
//  ProductDetailVM.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import Foundation
import RxSwift

protocol ProductDetailVMProtocol: AdditionalProductsVMProtocol,
                                  CartBottomViewDelegate {
    var viewState: BehaviorSubject<ViewState> { get set }
    var productDetail: ProductDetail? { get set }
    
    func startFetch()
    func addToCart()
    func decreaceInCart()
    func deleteFromCart()
}


class ProductDetailVM: ProductDetailVMProtocol {
    
    var viewState: BehaviorSubject<ViewState>
    var productDetail: ProductDetail?
    var additionalProducts: [Product] = []
    private var router: RouterProtocol

    
    init(router: RouterProtocol) {
        self.router = router
        self.viewState = .init(value: .initial)
    }
    
    
    public func startFetch() {
        self.viewState.onNext(.loading)
        
        Api.v1.getProductData().responseData { [weak self] (response) in
            switch response.result {
            case .success(let resultJSON):
                sleep(2)
                let resultData = try? JSONDecoder().decode(ProductDetail.self, from: resultJSON)
                guard let productDetail = resultData else {
                    print("Ошибка декодирования карточки товара")
                    return
                }
                self?.productDetail = productDetail
                self?.additionalProducts = productDetail.additionalProducts
                self?.viewState.onNext(.success)
            case .failure(let error):
                print(error.localizedDescription)
                self?.viewState.onNext(.failure)
            }
        }
    }
    
    
    func addToCart() {
        if let product = productDetail?.detailToProduct() {
            Cart.shared.addToCart(product: product)
        }
    }
    
    func decreaceInCart() {
        if let product = productDetail?.detailToProduct() {
            Cart.shared.decreaceInCart(product: product)
        }
    }
    
    func deleteFromCart() {
        if let product = productDetail?.detailToProduct() {
            Cart.shared.deleteFromCart(product: product)
        }
    }
    
    
    // MARK: - ProductCell Delegate
    func showProductDetailVC(id: Int) {
        // todo
    }
    
    func addToCart(product: Product) {
        Cart.shared.addToCart(product: product)
    }
    
    func decreaceInCart(product: Product) {
        Cart.shared.decreaceInCart(product: product)
    }
    
    func deleteFromCart(product: Product) {
        Cart.shared.deleteFromCart(product: product)
    }
    
    
    // MARK: - CartBottomView Delegate
    func showCartVC() {
        router.pushCartVC()
    }
}
