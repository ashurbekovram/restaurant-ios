//
//  ProductImageCVCell.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 05.12.2020.
//

import UIKit

class ProductImageCVCell: UICollectionViewCell {
    
    static var identifier = "ProductImageCVCell"
    
    @IBOutlet weak var productImage: UIImageView!
}
