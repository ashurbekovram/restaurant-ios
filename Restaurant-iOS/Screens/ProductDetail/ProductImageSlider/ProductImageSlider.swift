//
//  ProductImageSlider.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 05.12.2020.
//

import UIKit
import SDWebImage

protocol ProductImageSliderDelegate: class {
    func didSelectImage(_ position: Int, images: [String])
}


class ProductImageSlider: UIView {
    
    @IBOutlet weak var pageControl: UIPageControl! {
        didSet {
            pageControl.currentPageIndicatorTintColor = UIColor.RestColors.blue
            pageControl.pageIndicatorTintColor = UIColor.RestColors.lightGray
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            let layout = UICollectionViewFlowLayout()
            layout.estimatedItemSize = .zero
            layout.scrollDirection = .horizontal
            collectionView.collectionViewLayout = layout
            
            collectionView.showsHorizontalScrollIndicator = false
            collectionView.isPagingEnabled = true
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    
    private var images: [String] = []
    private var delegate: ProductImageSliderDelegate?
    
    func setImages(images: [String], delegate: ProductImageSliderDelegate?) {
        self.images = images
        self.delegate = delegate
        self.collectionView.reloadData()
        pageControl.numberOfPages = images.count
        pageControl.currentPage = 0
    }
}



extension ProductImageSlider: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.productImageCVCell, for: indexPath)!
                
        let imageUrl = URL(string: images[indexPath.item])
        let placeholderImage = UIImage(named: "productPlaceholder")
        cell.productImage.sd_setImage(with: imageUrl,
                                      placeholderImage: placeholderImage)
        return cell
    }
    
    
}

extension ProductImageSlider: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.bounds.size
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageNumber = round(targetContentOffset.pointee.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
}
