//
//  CartVM.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 07.12.2020.
//

import Foundation
import RxSwift

protocol CartVMProtocol: AdditionalProductsVMProtocol {
    var viewState: BehaviorSubject<ViewState> { get set }
    var cartProducts: [CartProduct] { get set }
    var fullPrice: Int { get set }
    
    func makeOrder()
}


class CartVM: CartVMProtocol {
    
    var viewState: BehaviorSubject<ViewState>
    var cartProducts: [CartProduct] = []
    
    // Говнокод
    var fullPrice: Int = 0 { didSet { viewState.onNext(.success)}}
    
    var additionalProducts: [Product] = []
    /*  [
            Product(id: 601, name: "test1", image: "", price: 10, oldPrice: nil, rating: nil),
            Product(id: 602, name: "test2", image: "", price: 20, oldPrice: nil, rating: nil),
            Product(id: 603, name: "test3", image: "", price: 30, oldPrice: nil, rating: nil),
            Product(id: 604, name: "test4", image: "", price: 40, oldPrice: nil, rating: nil),
        ]*/
    private var router: RouterProtocol
    private var disposeBag = DisposeBag()
    
    init(router: RouterProtocol) {
        self.router = router
        self.viewState = .init(value: .initial)
        self.loadProductsFromCart()
        self.loadCartFromNetwork()
        self.subscribe()
    }
    
    
    private func loadCartFromNetwork() {
        Api.v1.getCart().responseData { [weak self] (response) in
            switch response.result {
            case .success(let resultJSON):
                let resultData = try? JSONDecoder().decode(CartModel.self, from: resultJSON)
                guard let cartData = resultData else {
                    print("Ошибка декодирования корзины")
                    return
                }
                self?.additionalProducts = cartData.recomendProduct
                self?.viewState.onNext(.success)
            case .failure(let error):
                print(error.localizedDescription)
                self?.viewState.onNext(.failure)
            }
        }
    }
    
    // Говнокод
    private func subscribe() {
        Cart.shared.fullPrice.subscribe(onNext: { [self] (fullPrice) in
            self.fullPrice = fullPrice
        }).disposed(by: disposeBag)
    }
    
    
    private func loadProductsFromCart() {
        self.viewState.onNext(.loading)
        self.cartProducts = Cart.shared.products
        self.viewState.onNext(.success)
    }
    
    func makeOrder() {
        switch UserData.shared.orderType {
        case .delivery:
            router.pushDeliveryVC()
        case .reserveWithOrder:
            router.pushThanksVC()
        default:
            break
        }
    }
    
    
    // MARK: - ProductCellDelegate
    
    func showProductDetailVC(id: Int) {
        // todo
    }
    
    func addToCart(product: Product) {
        Cart.shared.addToCart(product: product)
        loadProductsFromCart()
    }
    
    func decreaceInCart(product: Product) {
        Cart.shared.decreaceInCart(product: product)
        loadProductsFromCart()
    }
    
    func deleteFromCart(product: Product) {
        Cart.shared.deleteFromCart(product: product)
        loadProductsFromCart()
    }
}
