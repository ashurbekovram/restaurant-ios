//
//  AdditionalProductsTVCell.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 20.12.2020.
//

import UIKit

class AdditionalProductsTVCell: UITableViewCell {
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



extension AdditionalProductsTVCell: T.Configurable {
    
    typealias Ref = [Product]
    typealias Delegate = AdditionalProductsVMProtocol
    
    static var attributes: T.CellAttributes {
        T.CellAttributes(R.reuseIdentifier.additionalProductsTVCell.identifier)
    }
    
    func config(_ additionalProducts: [Product]!, delegate: AdditionalProductsVMProtocol?) {
        let view = AdditionalProductsView.assemble(title: "Рекомендуем к заказу", viewModel: delegate!)
        stackView.subviews.forEach({ $0.removeFromSuperview()})
        stackView.addArrangedSubview(view)
    }
}
