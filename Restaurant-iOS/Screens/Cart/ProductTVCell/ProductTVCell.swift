//
//  ProductTVCell.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 07.12.2020.
//

import UIKit
import SDWebImage
import RxSwift


class ProductTVCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    private var cartProduct: CartProduct!
    private var delegate: ProductCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDesign()
    }
    
    
    @IBAction func minusAction(_ sender: Any) {
        delegate?.decreaceInCart(product: cartProduct.product)
    }
    
    
    @IBAction func plusAction(_ sender: Any) {
        delegate?.addToCart(product: cartProduct.product)
    }
    
    
    @IBAction func deleteAction(_ sender: Any) {
        delegate?.deleteFromCart(product: cartProduct.product)
    }
    
    
    private func setDesign() {
        productNameLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        priceLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        countLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
        deleteButton.setImage(R.image.iconClose(), for: .normal)
        deleteButton.tintColor = .black
        
        plusButton.layer.borderWidth = 1
        plusButton.layer.borderColor = R.color.gray06()?.cgColor
        plusButton.layer.cornerRadius = 4
        
        plusButton.setTitle(.none, for: .normal)
        plusButton.setImage(R.image.iconPlus(), for: .normal)
        plusButton.tintColor = R.color.orange()
        
        minusButton.layer.borderWidth = 1
        minusButton.layer.borderColor = R.color.gray06()?.cgColor
        minusButton.layer.cornerRadius = 4
        
        minusButton.setTitle(.none, for: .normal)
        minusButton.setImage(R.image.iconMinus(), for: .normal)
    }
    
    
    private func setMinusButtonColor() {
        if self.cartProduct.count > 1 {
            minusButton.tintColor = .black
        } else {
            minusButton.tintColor = R.color.gray32()
        }
    }
}



extension ProductTVCell: T.Configurable {
    
    typealias Ref = CartProduct
    typealias Delegate = ProductCellDelegate
    
    
    static var attributes: T.CellAttributes {
        T.CellAttributes(R.reuseIdentifier.productTVCell.identifier)
    }
    
    
    func config(_ cartProduct: CartProduct!, delegate: ProductCellDelegate?) {
        self.cartProduct = cartProduct
        self.delegate = delegate
        
        let imageURL = URL(string: cartProduct.product.image)
        self.productImageView.sd_setImage(with: imageURL,
                                     placeholderImage: R.image.productPlaceholder())
        
        self.productNameLabel.text = cartProduct.product.name
        self.priceLabel.text = "\(cartProduct.product.price)"
        self.countLabel.text = "\(cartProduct.count)"
        
        setMinusButtonColor()
    }
}

