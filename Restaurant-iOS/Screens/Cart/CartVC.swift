//
//  CartVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 07.12.2020.
//

import UIKit
import RxSwift

class CartVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var makeOrderButton: UIButton!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var fullPriceLabel: UILabel!
    
    
    var viewModel: CartVMProtocol!
    private let disposeBag = DisposeBag()
    private var dataSource: T.DataSource? {
        didSet {
            self.tableView.dataSource = dataSource
            self.tableView.delegate = dataSource
            self.tableView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Оформление заказа"
        subscribeOnViewModel()
        registerCells()
        setDesign()
    }
    
    
    @IBAction func makeOrderAction(_ sender: Any) {
        viewModel.makeOrder()
    }
    
    
    private func subscribeOnViewModel() {
        viewModel.viewState.subscribe(onNext: { [ weak self ] (viewState) in
            switch viewState {
            case .initial:
                print("cartVC initial")
            case .loading:
                print("cartVC loading")
                self?.showLoadIndicator()
            case .success:
                print("cartVC success")
                self?.setDataSource()
                self?.setLabels()
                self?.hideLoadIndicator()
            case .failure:
                print("cartVC failure")
                self?.hideLoadIndicator()
            }
        }).disposed(by: disposeBag)
    }
    
    
    private func registerCells() {
        tableView.register(R.nib.productTVCell)
        tableView.register(R.nib.additionalProductsTVCell)
    }
    
    
    private func setDesign() {
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        
        self.productCountLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        self.productCountLabel.textColor = .black
        
        self.fullPriceLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        self.fullPriceLabel.textColor = .black
        
        self.makeOrderButton.setCustomBG(bgColor: UIColor.RestColors.blue)
        self.makeOrderButton.setCustomTitle(title: "Заказать", titleColor: .white, fontSize: 16, fontWeight: .medium)
    }
    
    
    private func setLabels() {
        self.fullPriceLabel.text = "\(viewModel.fullPrice) ₽"
        self.productCountLabel.text = "\(viewModel.cartProducts.count) позиции"
    }
    
    
    private func setDataSource() {
        let cartProductsSection = T.Section()
        let additionalProductsSection = T.Section()
        
        for product in viewModel.cartProducts {
            cartProductsSection.cells.append(ProductTVCell.assemble(product, delegate: viewModel))
        }
        
        additionalProductsSection.cells.append(AdditionalProductsTVCell.assemble(nil, delegate: viewModel))
        
        dataSource = T.DataSource([cartProductsSection, additionalProductsSection])
    }
}
