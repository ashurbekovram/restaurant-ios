//
//  AddUserDataVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 23.12.2020.
//

import UIKit
import FittedSheets

class AddUserDataVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var formStackView: UIStackView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var dateView: UIView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var dateButton: UIButton!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var makeOrderButton: UIButton!
    @IBOutlet weak var reserveTableButton: UIButton!
    
    
    var viewModel: AddUserDataVMProtocol!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ваши данные"
        
        self.nameTextField.delegate = self
        self.phoneTextField.delegate = self
        
        self.nameTextField.addTarget(self, action: #selector(setName), for: .editingChanged)
        self.phoneTextField.addTarget(self, action: #selector(setPhone), for: .editingChanged)
        
        setDesign()
        setData()
        registerForKeyboardNotifications()
    }
    
    
    // MARK: - IB Actions
    @IBAction func chooseDateAction(_ sender: Any) {
        view.subviews.forEach({ if $0 is UITextField { $0.resignFirstResponder() } })
        
        // TODO: - Заменить на время с апи
        let dates = ["Завтра", "5 января", "6 января", "7 января"]
        let times = ["10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00"]
        
        let selectDateVC = R.storyboard.selectDateVC.selectDateVC()!
        selectDateVC.configure(delegate: self, dateArray: dates, timeArray: times)
        let sheetController = SheetViewController(controller: selectDateVC, sizes: [.intrinsic])
        self.present(sheetController, animated: true, completion: nil)
    }
    
    
    @IBAction func makePreOrderAction(_ sender: Any) {
        checkAllFieldsAppended()
        viewModel.makePreOrder()
    }
    
    
    @IBAction func reserveTableAction(_ sender: Any) {
        checkAllFieldsAppended()
        viewModel.reserveTable()
    }
    
    
    // MARK: - Private funcs
    private func setData() {
        self.nameTextField.text = viewModel.name
        self.phoneTextField.text = viewModel.phone
        
        if !viewModel.date.isEmpty {
            self.dateButton.setTitle(viewModel.date, for: .normal)
            self.dateButton.setTitleColor(.black, for: .normal)
        }
    }
    
    
    private func setDesign() {
        var contentInset: UIEdgeInsets = self.scrollView.contentInset
        contentInset.top = (scrollView.frame.height / 2) - (formStackView.frame.height / 2)
        scrollView.contentInset = contentInset
        
        nameView.setCustomBorder(color: R.color.gray06())
        phoneView.setCustomBorder(color: R.color.gray06())
        dateView.setCustomBorder(color: R.color.gray06())
        
        nameTextField.borderStyle = .none
        nameTextField.placeholder = "Как к вам обращаться"
        
        phoneTextField.borderStyle = .none
        phoneTextField.placeholder = "Телефон"
        
        dateButton.tintColor = .black
        dateButton.setImages(right: R.image.iconClock(), titleInset: 0, imageSideInset: 0)
        dateButton.setCustomTitle(title: "Дата и время", titleColor: .placeholderText, fontSize: 14, fontWeight: .regular)
        
        makeOrderButton.setCustomBorder(color: R.color.blue())
        makeOrderButton.setCustomTitle(title: "Оформить предзаказ",
                                       titleColor: R.color.blue(),
                                       fontSize: 16,
                                       fontWeight: .medium)
        
        reserveTableButton.setCustomBG(bgColor: R.color.orange())
        reserveTableButton.setCustomTitle(title: "Забронировать стол",
                                       titleColor: .white,
                                       fontSize: 16,
                                       fontWeight: .medium)
    }
    
    
    private func checkNameAppended() {
        if viewModel.name.isEmpty {
            self.nameView.setCustomBorder(color: R.color.red())
        } else {
            self.nameView.setCustomBorder(color: R.color.gray06())
        }
    }
    
    
    private func checkPhoneAppended() {
        if viewModel.phone.isEmpty {
            self.phoneView.setCustomBorder(color: R.color.red())
        } else {
            self.phoneView.setCustomBorder(color: R.color.gray06())
        }
    }
    
    
    private func checkDateAppended() {
        if viewModel.date.isEmpty {
            self.dateView.setCustomBorder(color: R.color.red())
            self.dateButton.setTitle("Дата и время", for: .normal)
            self.dateButton.setTitleColor(.placeholderText, for: .normal)
        } else {
            self.dateView.setCustomBorder(color: R.color.gray06())
            self.dateButton.setTitle(viewModel.date, for: .normal)
            self.dateButton.setTitleColor(.black, for: .normal)
        }
    }
    
    
    private func checkAllFieldsAppended() {
        checkNameAppended()
        checkPhoneAppended()
        checkDateAppended()
    }
    
    
    @objc private func setName() {
        viewModel.name = nameTextField.text ?? ""
        checkNameAppended()
    }
    
    
    @objc private func setPhone() {
        viewModel.phone = phoneTextField.text ?? ""
        checkPhoneAppended()
    }
    
    
    // MARK: - Keyboard
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc private func kbWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardFrame: CGRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        var contentInset: UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    
    @objc private func kbWillHide() {
        //let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset.bottom = 0
    }
}



// MARK: - SelectDate Delegate
extension AddUserDataVC: SelectDateDelegate {
    
    func selectDate(date: String) {
        self.viewModel.date = date
        checkDateAppended()
    }
}



// MARK: - TextField Delegate
extension AddUserDataVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
