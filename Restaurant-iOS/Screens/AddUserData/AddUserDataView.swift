//
//  AddUserDataView.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 02.01.2021.
//



import SwiftUI

struct AddUserDataView: View {
    
    @State var name: String = ""
    @State var phone: String = ""
    @State var date: String = ""
    
    var bool = false
    var viewModel: AddUserDataVMProtocol
    
    
    let errorFieldLabel: some View = Text("Заполните поле")
        .foregroundColor(.red)
        .padding(.trailing, 16)
    
    
    var body: some View {
        
        GeometryReader { geometry in
            
            ScrollView(.vertical, showsIndicators: true) {
                
                VStack(alignment: .center, spacing: 12) {
                    
                    Spacer()
                    
                    ZStack(alignment: .leading) {
                        
                        if self.name.isEmpty {
                            HStack {
                                Text("Как к вам обращаться")
                                    .foregroundColor(Color(R.color.gray32.name))
                                    .padding(.horizontal, 16)
                                
                                Spacer()
                                
                                errorFieldLabel
                            }
                        }
                        
                        
                        TextField("", text: $name)
                            .padding(.horizontal, 16.0)
                            .frame(height: 48)
                            .addBorder(Color(R.color.gray06.name), cornerRadius: 8)
                        
                    }
                    
                    ZStack(alignment: .leading) {
                        
                        if self.phone.isEmpty {
                            HStack {
                                Text("Телефон")
                                    .foregroundColor(Color(R.color.gray32.name))
                                    .padding(.horizontal, 16)
                                
                                Spacer()
                                
                                errorFieldLabel
                            }
                        }
                        
                        TextField("", text: $phone)
                            .padding(.horizontal, 16.0)
                            .frame(height: 48)
                            .addBorder(Color(R.color.gray06.name), cornerRadius: 8)
                        
                    }
                    
                    ZStack(alignment: .trailing) {
                        Button(action: {
                            
                        }, label: {
                            HStack {
                                Text("Дата")
                                    .foregroundColor(Color(R.color.gray32.name))
                                Spacer()
                            }
                        })
                        .padding(.horizontal, 16.0)
                        .frame(height: 48)
                        .addBorder(Color(R.color.gray06.name), cornerRadius: 8)
                        
                        Image(R.image.iconClock.name)
                            .resizable()
                            .frame(width: 20, height: 20)
                            .padding(.trailing, 16)
                    }
                    
                    Spacer()
                    
                    createBottomView()
                    
                }
                .padding(.horizontal, 16)
                .frame(width: geometry.size.width)
                .frame(minHeight: geometry.size.height)
            }
        }
    }
    
    
    private func createBottomView() -> some View {
        return VStack(spacing: 16) {
            
            Button(action: { }, label: {
                HStack {
                    Spacer()
                    Text("Сделать предзаказ")
                    Spacer()
                }
            })
            .frame(height: 48)
            .background(Color.white)
            .foregroundColor(Color(R.color.blue.name))
            .font(.system(size: 16, weight: .medium))
            .addBorder(Color(R.color.blue.name), cornerRadius: 8)
            
            Button(action: { }, label: {
                HStack {
                    Spacer()
                    Text("Забронировать стол")
                    Spacer()
                }
            })
            .frame(height: 48)
            .background(Color(R.color.orange.name))
            .foregroundColor(.white)
            .font(.system(size: 16, weight: .medium))
            .cornerRadius(8)
        }
        .padding(.vertical, 16)
    }
}



struct AddUserDataPreviews: PreviewProvider {
    
    static var previews: some View {
        AddUserDataView(viewModel: AddUserDataVM(router: nil))
    }
}



extension View {
    
    public func addBorder<S>(_ content: S, width: CGFloat = 1, cornerRadius: CGFloat) -> some View where S : ShapeStyle {
        let roundedRect = RoundedRectangle(cornerRadius: cornerRadius)
        return clipShape(roundedRect)
            .overlay(roundedRect.strokeBorder(content, lineWidth: width))
    }
}


