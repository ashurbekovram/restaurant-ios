//
//  SelectDateVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 03.01.2021.
//

import UIKit

protocol SelectDateDelegate {
    func selectDate(date: String)
}


class SelectDateVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIPickerView!
    @IBOutlet weak var selectButton: UIButton!
    
    private var delegate: SelectDateDelegate?
    private var dateArray: [String] = []
    private var timeArray: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.dataSource = self
        datePicker.delegate = self
        
        setDesign()
    }
    
    
    @IBAction func selectDateAction(_ sender: Any) {
        delegate?.selectDate(date: selectedDate())
        self.dismiss(animated: true, completion: nil)
    }
    
    private func selectedDate() -> String {
        let dateIndex = datePicker.selectedRow(inComponent: 0)
        let timeIndex = datePicker.selectedRow(inComponent: 1)
        let dateString = dateArray[dateIndex] + " " + timeArray[timeIndex]
        return dateString
    }
    
    
    public func configure(delegate: SelectDateDelegate?, dateArray: [String], timeArray: [String]) {
        self.delegate = delegate
        self.dateArray = dateArray
        self.timeArray = timeArray
    }
    
    
    private func setDesign() {
        titleLabel.text = "Дата и время брони"
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        selectButton.setCustomBG(bgColor: R.color.blue())
        selectButton.setCustomTitle(title: selectedDate(), titleColor: .white, fontSize: 16, fontWeight: .medium)
    }
    
    
    // MARK: - PickerView DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return dateArray.count
        } else {
            return timeArray.count
        }
    }
    
    
    // MARK: - PickerView Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return dateArray[row]
        } else {
            return timeArray[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectButton.setTitle(selectedDate(), for: .normal)
    }
}
