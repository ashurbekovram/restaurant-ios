//
//  AddUserDataVM.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 23.12.2020.
//

import Foundation


protocol AddUserDataVMProtocol {
    var name: String { get set }
    var phone: String { get set }
    var date: String { get set }
    
    func makePreOrder()
    func reserveTable()
}


class AddUserDataVM: AddUserDataVMProtocol {
    
    private let router: RouterProtocol?
    
    var name: String = UserData.shared.name
    var phone: String = UserData.shared.phone
    var date: String = UserData.shared.date
    
    init (router: RouterProtocol?) {
        self.router = router
    }
    
    func makePreOrder() {
        Cart.shared.clearCart()
        UserData.shared.orderType = .reserveWithOrder
        self.router?.pushMenuVC()
    }
    
    func reserveTable() {
        UserData.shared.orderType = .reserve
        self.router?.pushThanksVC()
    }
}
