//
//  DeliveryVM.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 03.01.2021.
//

import Foundation


protocol DeliveryVMProtocol {
    var name: String { get set }
    var phone: String { get set }
    var street: String { get set }
    var house: String { get set }
    var apartment: String { get set }
    
    func makeOrder()
}

class DeliveryVM: DeliveryVMProtocol {
    
    private let router: RouterProtocol?
    
    var name: String = UserData.shared.name
    var phone: String = UserData.shared.phone
    var house: String = ""
    var street: String = ""
    var apartment: String = ""
    
    
    init (router: RouterProtocol?) {
        self.router = router
    }
    
    func makeOrder() {
        guard !name.isEmpty && !phone.isEmpty && !street.isEmpty && !house.isEmpty
        else { return }
        
        Cart.shared.clearCart()
        UserData.shared.orderType = .delivery
        self.router?.pushThanksVC()
    }
}
