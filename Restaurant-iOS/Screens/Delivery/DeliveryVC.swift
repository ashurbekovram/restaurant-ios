//
//  DeliveryVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 03.01.2021.
//

import UIKit

class DeliveryVC: UIViewController {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var formStackView: UIStackView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var streetView: UIView!
    @IBOutlet weak var houseView: UIView!
    @IBOutlet weak var apartmentView: UIView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var houseTextField: UITextField!
    @IBOutlet weak var apartmentTextField: UITextField!
    
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var emtyFieldsErrorView: UIView!
    @IBOutlet weak var emtyFieldsErrorLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var makeOrderButton: UIButton!
    
    
    var viewModel: DeliveryVMProtocol!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ваши данные"
        
        self.nameTextField.delegate = self
        self.phoneTextField.delegate = self
        self.streetTextField.delegate = self
        self.houseTextField.delegate = self
        self.apartmentTextField.delegate = self
        
        self.nameTextField.addTarget(self, action: #selector(setName), for: .editingChanged)
        self.phoneTextField.addTarget(self, action: #selector(setPhone), for: .editingChanged)
        self.streetTextField.addTarget(self, action: #selector(setStreet), for: .editingChanged)
        self.houseTextField.addTarget(self, action: #selector(setHouse), for: .editingChanged)
        self.apartmentTextField.addTarget(self, action: #selector(setApartment), for: .editingChanged)
        
        registerForKeyboardNotifications()
        setDesign()
        setData()
    }
    
    
    
    @IBAction func makeOrderAction(_ sender: Any) {
        checkAllFieldsAppended()
        viewModel.makeOrder()
    }
    
    
    // MARK: - Private funcs
    private func setData() {
        self.nameTextField.text = viewModel.name
        self.phoneTextField.text = viewModel.phone
    }
    
    
    private func setDesign() {
        var contentInset: UIEdgeInsets = self.scrollView.contentInset
        contentInset.top = (scrollView.frame.height / 2) - (formStackView.frame.height / 1.5)
        scrollView.contentInset = contentInset
        
        nameView.setCustomBorder(color: R.color.gray06())
        phoneView.setCustomBorder(color: R.color.gray06())
        streetView.setCustomBorder(color: R.color.gray06())
        houseView.setCustomBorder(color: R.color.gray06())
        apartmentView.setCustomBorder(color: R.color.gray06())
        
        nameTextField.borderStyle = .none
        nameTextField.placeholder = "Как к вам обращаться"
        
        phoneTextField.borderStyle = .none
        phoneTextField.placeholder = "Телефон"
        
        streetTextField.borderStyle = .none
        streetTextField.placeholder = "Улица"
        
        houseTextField.borderStyle = .none
        houseTextField.placeholder = "Номер дома"
        
        apartmentTextField.borderStyle = .none
        apartmentTextField.placeholder = "Номер квартиры"
        
        infoLabel.text = "*Доставка осуществляется только по Махачкале"
        infoLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        infoLabel.textColor = .placeholderText
        
        emtyFieldsErrorLabel.isHidden = true
        emtyFieldsErrorLabel.text = "Заполните поля"
        emtyFieldsErrorLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        emtyFieldsErrorLabel.textColor = R.color.red()
        
        makeOrderButton.setCustomBG(bgColor: R.color.blue())
        makeOrderButton.setCustomTitle(title: "Оформить доставку",
                                       titleColor: .white,
                                       fontSize: 16,
                                       fontWeight: .medium)
    }
    
    
    private func checkNameAppended() {
        if viewModel.name.isEmpty {
            self.nameView.setCustomBorder(color: R.color.red())
            self.emtyFieldsErrorLabel.isHidden = false
        } else {
            self.nameView.setCustomBorder(color: R.color.gray06())
            self.emtyFieldsErrorLabel.isHidden = true
        }
    }
    
    
    private func checkPhoneAppended() {
        if viewModel.phone.isEmpty {
            self.phoneView.setCustomBorder(color: R.color.red())
            self.emtyFieldsErrorLabel.isHidden = false
        } else {
            self.phoneView.setCustomBorder(color: R.color.gray06())
            self.emtyFieldsErrorLabel.isHidden = true
        }
    }
    
    
    private func checkStreetAppended() {
        if viewModel.street.isEmpty {
            self.streetView.setCustomBorder(color: R.color.red())
            self.emtyFieldsErrorLabel.isHidden = false
        } else {
            self.streetView.setCustomBorder(color: R.color.gray06())
            self.emtyFieldsErrorLabel.isHidden = true
        }
    }
    
    
    private func checkHouseAppended() {
        if viewModel.house.isEmpty {
            self.houseView.setCustomBorder(color: R.color.red())
            self.emtyFieldsErrorLabel.isHidden = false
        } else {
            self.houseView.setCustomBorder(color: R.color.gray06())
            self.emtyFieldsErrorLabel.isHidden = true
        }
    }
    
    
    private func checkAllFieldsAppended() {
        checkNameAppended()
        checkPhoneAppended()
        checkStreetAppended()
        checkHouseAppended()
    }
    
    
    // MARK: - Targets
    @objc private func setName() {
        viewModel.name = nameTextField.text ?? ""
        checkNameAppended()
    }
    
    
    @objc private func setPhone() {
        viewModel.phone = phoneTextField.text ?? ""
        checkPhoneAppended()
    }
    
    
    @objc private func setStreet() {
        viewModel.street = streetTextField.text ?? ""
        checkStreetAppended()
    }
    
    
    @objc private func setHouse() {
        viewModel.house = houseTextField.text ?? ""
        checkHouseAppended()
    }
    
    
    @objc private func setApartment() {
        viewModel.apartment = apartmentTextField.text ?? ""
        checkHouseAppended()
    }
    
    
    // MARK: - Keyboard
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc private func kbWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardFrame: CGRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        var contentInset: UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    
    @objc private func kbWillHide() {
        //let contentInset: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset.bottom = 0
    }
}



// MARK: - TextField Delegate
extension DeliveryVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
