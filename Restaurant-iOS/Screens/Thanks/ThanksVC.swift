//
//  ThanksVC.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 03.01.2021.
//

import UIKit


class ThanksVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var goHomeButton: UIButton!
    
    
    var router: RouterProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.view.backgroundColor = .white
        self.titleLabel.textColor = .black
        self.titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        
        switch UserData.shared.orderType {
        case .none: break
        case .delivery:
            self.titleLabel.text = "Ваша заявка принята, спасибо за заказ. Ожидайте звонка."
        case .onPoint:
            self.titleLabel.text = "Ваш заказ уже обрабатывается на кассе. Приятного аппетита!"
        case .reserve:
            self.titleLabel.text = "Ваша заявка на бронирование стола принята. Ожидайте звонка"
        case .reserveWithOrder:
            self.titleLabel.text = "Ваша заявка на бронирование с предзаказом принята. Ожидайте звонка"
        }
        
        self.goHomeButton.setCustomBG(bgColor: R.color.blue())
        self.goHomeButton.setCustomTitle(title: "Вернуться на главную",
                                         titleColor: .white,
                                         fontSize: 16,
                                         fontWeight: .medium)
    }
    
    
    @IBAction func goHomeAction(_ sender: Any) {
        router?.popToRoot()
    }
}
