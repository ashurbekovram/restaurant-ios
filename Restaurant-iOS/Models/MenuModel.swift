//
//  MenuModel.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import Foundation

struct Category: Codable {
    let id: Int
    let categoryName: String
    let categoryIcon: String?
    let items: [Product]?
}

