//
//  CartModel.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 19.12.2020.
//

import Foundation

struct CartProduct: Codable {
    var product: Product
    var count: Int
}


struct CartModel: Codable {
    //var orderedProduct: [Product]
    var recomendProduct: [Product]
}
