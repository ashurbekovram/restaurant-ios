//
//  TableModel.swift
//  Restaurant-iOS
//
//  Created by Асельдер on 10.12.2020.
//

import Foundation

struct Table: Codable {
    let tableId: Int
    let tableImages: [String]
    let personsCount: Int
    let tableNumber: Int
    
//    let veranda: Bool?
//    let tableByWindow: Bool?
//    let tableValidate: Bool?
}
