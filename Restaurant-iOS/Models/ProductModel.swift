//
//  ProductModel.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 28.11.2020.
//

import Foundation

struct Product: Codable {
    let id: Int
    let name: String
    let image: String
    let price: Int
    let oldPrice: Int?
    let rating: Double?
}
