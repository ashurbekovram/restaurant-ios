//
//  ProductDetailModel.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 07.12.2020.
//

import Foundation

struct ProductDetail: Codable {
    let id: Int
    let name: String
    let description: String?
    let image: [String]
    let price: Int
    let oldPrice: Int?
    let rating: Double?
    let productInfo: [ProductInfo]?
    let additionalProducts: [Product]
    
    
    struct ProductInfo: Codable {
        let id: Int
        let name: String
        let value: Int
    }
    
    
    func detailToProduct() -> Product {
        let product = Product(id: id,
                              name: name,
                              image: image.first ?? "",
                              price: price,
                              oldPrice: oldPrice,
                              rating: rating)
        return product
    }
}
