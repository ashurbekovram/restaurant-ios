//
//  ViewState.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 17.11.2020.
//

import Foundation

enum ViewState {
    case initial
    case loading
    case success
    case failure
}
