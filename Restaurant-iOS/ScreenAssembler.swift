//
//  ScreenAssembler.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import UIKit

protocol ScreenAssemblerProtocol {
    func assembleMainScreen(router: RouterProtocol) -> UIViewController
    func assembleMenuScreen(router: RouterProtocol) -> UIViewController
    func assembleProductDetailScreen(router: RouterProtocol) -> UIViewController
    func assembleCartScreen(router: RouterProtocol) -> UIViewController
    func assembleTableReservationScreen(router: RouterProtocol) -> UIViewController
    func assembleAddUserDataScreen(router: RouterProtocol) -> UIViewController
    func assembleDeliveryScreen(router: RouterProtocol) -> UIViewController
    func assembleThanksScreen(router: RouterProtocol) -> UIViewController
}


class ScreenAssembler: ScreenAssemblerProtocol {
    
    func assembleMainScreen(router: RouterProtocol) -> UIViewController {
        let vm = MainVM(router: router)
        let sb = UIStoryboard(name: "MainVC", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        vc.viewModel = vm
        return vc
    }
    
    
    func assembleMenuScreen(router: RouterProtocol) -> UIViewController {
        let vm = MenuVM(router: router)
        let sb = UIStoryboard(name: "MenuVC", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        vc.viewModel = vm
        return vc
    }
    
    
    func assembleProductDetailScreen(router: RouterProtocol) -> UIViewController {
        let vm = ProductDetailVM(router: router)
        let sb = UIStoryboard(name: "ProductDetailVC", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        vc.viewModel = vm
        return vc
    }
    
    
    func assembleCartScreen(router: RouterProtocol) -> UIViewController {
        let vm = CartVM(router: router)
        let sb = UIStoryboard(name: "CartVC", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        vc.viewModel = vm
        return vc
    }
    
    
    func assembleTableReservationScreen(router: RouterProtocol) -> UIViewController {
        let vm = TableReservationVM(router: router)
        let vc = R.storyboard.tableReservationVC.tableReservationVC()!
        vc.viewModel = vm
        return vc
    }
    
    
    func assembleAddUserDataScreen(router: RouterProtocol) -> UIViewController {
        let vm = AddUserDataVM(router: router)
        let vc = R.storyboard.addUserDataVC.addUserDataVC()!
        vc.viewModel = vm
        return vc
    }
    
    
    func assembleDeliveryScreen(router: RouterProtocol) -> UIViewController {
        let vm = DeliveryVM(router: router)
        let vc = R.storyboard.deliveryVC.deliveryVC()!
        vc.viewModel = vm
        return vc
    }
    
    
    func assembleThanksScreen(router: RouterProtocol) -> UIViewController {
        let vc = R.storyboard.thanksVC.thanksVC()!
        vc.router = router
        return vc
    }
}
