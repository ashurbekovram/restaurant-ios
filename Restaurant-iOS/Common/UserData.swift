//
//  UserData.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 03.01.2021.
//

import Foundation

enum OrderType: String {
    case none = "none"
    case onPoint = "onPoint"
    case delivery = "delivery"
    case reserve = "reserve"
    case reserveWithOrder = "reserveWithOrder"
}


class UserData {
    
    static let shared = UserData()
    
    var name: String = "" { didSet { UserDefaults.standard.set(name, forKey: nameKey) } }
    var phone: String = "" { didSet { UserDefaults.standard.set(phone, forKey: phoneKey) } }
    var date: String = "" { didSet { UserDefaults.standard.set(date, forKey: dateKey) } }
    var table: String = "" { didSet { UserDefaults.standard.set(table, forKey: tableKey) } }
    
    var orderType: OrderType = .none {
        didSet {
            UserDefaults.standard.set(orderType.rawValue, forKey: orderKey)
        }
    }
    
    private let nameKey = "rest_app_userdefaults_name_key"
    private let phoneKey = "rest_app_userdefaults_phone_key"
    private let dateKey = "rest_app_userdefaults_date_key"
    private let tableKey = "rest_app_userdefaults_table_key"
    private let orderKey = "rest_app_userdefaults_order_key"
    
    
    private init() {
        if let name = UserDefaults.standard.string(forKey: nameKey) { self.name = name }
        if let phone = UserDefaults.standard.string(forKey: phoneKey) { self.phone = phone }
        if let date = UserDefaults.standard.string(forKey: dateKey) { self.date = date }
        if let table = UserDefaults.standard.string(forKey: tableKey) { self.table = table }
        
        if let orderType = UserDefaults.standard.string(forKey: orderKey) {
            self.orderType = OrderType(rawValue: orderType) ?? .none
        }
        
        //UserDefaults.standard.removeObject(forKey: dateKey)
        //UserDefaults.standard.removeObject(forKey: tableKey)
    }
    
    
    private func resetOrderInfo() {
        self.date = ""
        self.table = ""
        self.orderType = .none
    }
    
    
    private func resetUserInfo() {
        self.name = ""
        self.phone = ""
    }
    
    
    // Пока не используется
    private func syncronize() {
        UserDefaults.standard.set(name, forKey: nameKey)
        UserDefaults.standard.set(phone, forKey: phoneKey)
        UserDefaults.standard.set(date, forKey: dateKey)
        UserDefaults.standard.set(table, forKey: tableKey)
    }
}
