//
//  Cart.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 07.12.2020.
//

import Foundation
import RxSwift

protocol ProductCellDelegate {
    func showProductDetailVC(id: Int)
    
    func addToCart(product: Product)
    func decreaceInCart(product: Product)
    func deleteFromCart(product: Product)
}


class Cart {
    
    static let shared = Cart()
    
    var fullPrice: BehaviorSubject<Int> = .init(value: 0)
    
    var products: [CartProduct] = [] {
        didSet {
            var _fullPrice = 0
            products.forEach({ (cartProduct) in
                _fullPrice += cartProduct.count * cartProduct.product.price
            })
            fullPrice.onNext(_fullPrice)
        }
    }
    
    private let key = "rest_app_userdefaults_cart_key"
    
    
    private init() {
        guard let data = UserDefaults.standard.data(forKey: key),
              let products = try? JSONDecoder().decode([CartProduct].self, from: data)
        else { return }
        self.products.append(contentsOf: products)
        
        var fullPrice = 0
        products.forEach( {fullPrice += $0.count * $0.product.price})
        self.fullPrice.onNext(fullPrice)
    }
    
    
    func isAddedToCart(product: Product) -> Bool {
        if let _ = products.firstIndex(where: {$0.product.id == product.id}) {
            return true
        } else {
            return false
        }
    }
    
    func addToCart(product: Product) {
        if let index = products.firstIndex(where: {$0.product.id == product.id}) {
            products[index].count += 1
            synchronize()
        } else {
            let newProduct = CartProduct(product: product, count: 1)
            products.append(newProduct)
            synchronize()
        }
    }
    
    func decreaceInCart(product: Product) {
        if let index = products.firstIndex(where: {$0.product.id == product.id}) {
            if products[index].count > 1 {
                products[index].count -= 1
                synchronize()
            } else {
                products.remove(at: index)
                synchronize()
            }
        } else {
            print("Элемента с данным id нет в корзине")
        }
    }
    
    func deleteFromCart(product: Product) {
        if let index = products.firstIndex(where: {$0.product.id == product.id}) {
            products.remove(at: index)
            synchronize()
        }
    }
    
    func clearCart() {
        products.removeAll()
        synchronize()
    }
    
    func checkCountOf(product: Product) -> Int {
        if let index = products.firstIndex(where: {$0.product.id == product.id}) {
            let count = products[index].count
            return count
        } else {
            return 0
        }
    }
    
    
    // MARK: - Syncronize
    private func synchronize() {
        guard let products = try? JSONEncoder().encode(products)
        else { return }
        UserDefaults.standard.set(products, forKey: key)
    }
}
