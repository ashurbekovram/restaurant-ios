//
//  T + Section.swift
//  Hoff
//
//  Created by Abdula Magomedov on 14.04.2020.
//  Copyright © 2020 WeLike. All rights reserved.
//

import UIKit

extension T {
    
    class Section {
        
        var cells: [Configurator]
        
        var titleForHeader: String?
        var titleForFooter: String?
        var viewForHeader: UIView?
        var heightForHeader: CGFloat = UITableView.automaticDimension
        
        var viewForFooter: UIView?
        var heightForFooter: CGFloat = UITableView.automaticDimension
        
        init(_ cells: [Configurator] = []) {
            self.cells = cells
        }
    }
}
