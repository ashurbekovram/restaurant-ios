//
//  T + DataSource.swift
//  Hoff
//
//  Created by Abdula Magomedov on 14.04.2020.
//  Copyright © 2020 WeLike. All rights reserved.
//

import UIKit

extension T {
    
    class DataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
        
        var sourceData: Any?
        var willDisplayLastCell: (() -> Void)?
        
        var sections: [Section]
        
        init(_ sections: [Section]) {
            self.sections = sections
        }
        
        init(_ cells: [T.Configurator]) {
            self.sections = [T.Section(cells)]
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return sections.count
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return sections[section].cells.count
        }
        
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return sections[section].titleForHeader
        }
        
        func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
            return sections[section].titleForFooter
        }
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            return sections[section].viewForFooter
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            return sections[section].viewForHeader
        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return sections[section].heightForFooter
        }
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return sections[section].heightForHeader
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return sections[indexPath.section].cells[indexPath.row].attributes.heightForRow(tableView, indexPath)
        }
        
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return sections[indexPath.section].cells[indexPath.row].attributes.heightForRow(tableView, indexPath)
        }
        
        func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            sections[indexPath.section].cells[indexPath.row].leadingSwipeActionsConfigurationForRow(at: indexPath, in: tableView)
        }
        
        func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            sections[indexPath.section].cells[indexPath.row].trailingSwipeActionsConfigurationForRow(at: indexPath, in: tableView)
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
            if indexPath.section == (sections.count - 1) && indexPath.row == (sections.last!.cells.count - 1) {
                willDisplayLastCell?()
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let configurator = sections[indexPath.section].cells[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: configurator.attributes.identifier, for: indexPath)
            
            configurator.config(cell: cell)
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            sections[indexPath.section].cells[indexPath.row].didSelect(in: tableView, at: indexPath)
        }
        
        func deleteCells(at indexPaths: [IndexPath]) {
            
            indexPaths.forEach { (indexPath) in
                self.sections[indexPath.section].cells.remove(at: indexPath.row)
            }
        }
        
        static var empty: DataSource { DataSource([Section([])]) }
    }
}

