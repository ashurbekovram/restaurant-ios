//
//  T.swift
//  Hoff
//
//  Created by Abdula Magomedov on 14.04.2020.
//  Copyright © 2020 WeLike. All rights reserved.
//

import UIKit

protocol T_Configurator {
    
    var attributes: T.CellAttributes { get set }
    
    func leadingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration?
    func trailingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration?
    func didSelect(in tableView: UITableView, at indexPath: IndexPath)
    
    func config(cell: UITableViewCell)
}

protocol T_Configurable {
    
    associatedtype Ref
    associatedtype Delegate
    
    static var attributes: T.CellAttributes { get }
    
    func config(_ ref: Ref!, delegate: Delegate?)
    func leadingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration?
    func trailingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration?
    func didSelect(in tableView: UITableView, at indexPath: IndexPath)
}

extension T_Configurable {
    
    static func assemble(_ ref: Ref? = nil, refClosure: (() -> Ref?)? = nil, delegate: Delegate? = nil) -> T.Configurator {
        T.Cell<Self>(Self.self, ref: ref, refClosure: refClosure, delegate: delegate)
    }
    
    func leadingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? { nil }
    func trailingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? { nil }
    func didSelect(in tableView: UITableView, at indexPath: IndexPath) {}
}


class T {
    typealias Configurable = T_Configurable
    typealias Configurator = T_Configurator
}
