//
//  T + Cell.swift
//  Hoff
//
//  Created by Abdula Magomedov on 14.04.2020.
//  Copyright © 2020 WeLike. All rights reserved.
//

import UIKit

extension T {
    
    //MARK: - Статичная ячейка. Без действий
    struct StaticCell: Configurator {
        
        var attributes: T.CellAttributes
        
        let delegate: Any? = nil
        let ref: Any? = nil
        
        func config(cell: UITableViewCell) {}
        
        func leadingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            nil
        }
        
        func trailingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            nil
        }
        
        func didSelect(in tableView: UITableView, at indexPath: IndexPath) {}
        
        init(_ cellIdentifier: String) {
            self.attributes = T.CellAttributes(cellIdentifier)
        }
        
        init(with attributes: T.CellAttributes) {
            self.attributes = attributes
        }
    }
    
    //MARK: - Неизменяемая ячейка. Можно настроить действие на выделение
    struct SimpleCell: Configurator {
        
        typealias DidSelectAction = () -> Void
        
        var attributes: T.CellAttributes
        
        let didSelectAction: DidSelectAction
        let delegate: Any? = nil
        let ref: Any? = nil
        
        func config(cell: UITableViewCell) {}
        
        func leadingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            nil
        }
        
        func trailingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            nil
        }
        
        func didSelect(in tableView: UITableView, at indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
            didSelectAction()
        }
        
        init(_ cellIdentifier: String, didSelect: @escaping DidSelectAction) {
            self.attributes = T.CellAttributes(cellIdentifier)
            self.didSelectAction = didSelect
        }
        
        init(with attributes: T.CellAttributes, didSelect: @escaping DidSelectAction) {
            self.attributes = attributes
            self.didSelectAction = didSelect
        }
    }
    
    //MARK: - Ячейка с типом Basic. Можно настроить title и действие на выделение
    struct BasicCell: Configurator {
        
        typealias DidSelectAction = () -> Void
        
        var attributes: T.CellAttributes
        
        let title: String
        let didSelectAction: DidSelectAction
        let delegate: Any? = nil
        let ref: Any? = nil
        
        func config(cell: UITableViewCell) {
            cell.textLabel?.text = title
        }
        
        func leadingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            nil
        }
        
        func trailingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            nil
        }
        
        func didSelect(in tableView: UITableView, at indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
            didSelectAction()
        }
        
        init(_ cellIdentifier: String, title: String, didSelect: @escaping DidSelectAction) {
            self.attributes = T.CellAttributes(cellIdentifier)
            self.didSelectAction = didSelect
            self.title = title
        }
    }
    
    //MARK: - Любая ячейка подписанная под протокол Cofigurable
    struct Cell<I: Configurable>: Configurator {
        
        typealias DidSelectAction = (_ tableView: UITableView, _ indexPath: IndexPath) -> Void
        
        var attributes: T.CellAttributes
        
        private let delegate: I.Delegate?
        private let ref: I.Ref?
        private let refClosure: (() -> I.Ref?)?
        
        private let didSelectAction: DidSelectAction?
        
        func config(cell: UITableViewCell) {
            
            if let refClosure = refClosure {
                (cell as? I)?.config(refClosure(), delegate: delegate)
            } else {
                (cell as? I)?.config(ref, delegate: delegate)
            }
        }
        
        func leadingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            (tableView.cellForRow(at: indexPath) as? I)?.leadingSwipeActionsConfigurationForRow(at: indexPath, in: tableView)
        }
        
        func trailingSwipeActionsConfigurationForRow(at indexPath: IndexPath, in tableView: UITableView) -> UISwipeActionsConfiguration? {
            (tableView.cellForRow(at: indexPath) as? I)?.trailingSwipeActionsConfigurationForRow(at: indexPath, in: tableView)
        }
        
        func didSelect(in tableView: UITableView, at indexPath: IndexPath) {
            
            if let didSelectAction = self.didSelectAction {
                didSelectAction(tableView, indexPath)
            } else {
                (tableView.cellForRow(at: indexPath) as? I)?.didSelect(in: tableView, at: indexPath)
            }
        }
        
        init(_: I.Type, ref: I.Ref? = nil, refClosure: (() -> I.Ref?)? = nil, delegate: I.Delegate? = nil) {
            
            self.attributes = I.attributes
            self.ref = ref
            self.refClosure = refClosure
            self.delegate = delegate
            self.didSelectAction = nil
        }
        
        init(_: I.Type, ref: I.Ref? = nil, refClosure: (() -> I.Ref?)? = nil, didSelectAction: @escaping DidSelectAction) {
            
            self.attributes = I.attributes
            self.ref = ref
            self.refClosure = refClosure
            self.delegate = nil
            self.didSelectAction = didSelectAction
        }
    }
}
