//
//  T + CellAttributes.swift
//  Hoff
//
//  Created by Abdula Magomedov on 14.04.2020.
//  Copyright © 2020 WeLike. All rights reserved.
//

import UIKit

extension T {
    
    struct CellAttributes {
        
        let identifier: String
        
        var heightForRow: (UITableView, IndexPath) -> CGFloat =  {_,_ in
            UITableView.automaticDimension
        }
        
        init(_ identifier: String, _ overrider: (inout CellAttributes) -> Void = {_ in }) {
            self.identifier = identifier
            overrider(&self)
        }
    }
}
