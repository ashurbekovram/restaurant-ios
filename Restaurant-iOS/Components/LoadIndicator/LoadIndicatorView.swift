//
//  LoadIndicatorView.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 05.12.2020.
//

import UIKit

class LoadIndicatorView: UIView {
    
    @IBOutlet weak var indicatorContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        indicatorContainer.layer.cornerRadius = indicatorContainer.frame.height / 2
    }
    
    
    static func assemblyWith(frame: CGRect) -> LoadIndicatorView {
        let view = R.nib.loadIndicatorView(owner: nil)!
        view.frame = frame
        return view
    }
}
