//
//  AdditionalProductCVCell.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 05.12.2020.
//

import UIKit
import SDWebImage

class AdditionalProductCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    
    private var product: Product!
    private var delegate: ProductCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDesign()
    }
    
    
    @IBAction func plusAction(_ sender: Any) {
        delegate?.addToCart(product: product)
    }
    
    
    func setData(product: Product, delegate: ProductCellDelegate) {
        self.product = product
        self.delegate = delegate
        
        nameLabel.text = product.name
        priceLabel.text = String(product.price) + " ₽"
        imageVIew.sd_setImage(
            with: URL(string: product.image),
            placeholderImage: UIImage(named: "productPlaceholder"))
    }
    
    
    private func setDesign() {
        contentView.setCustomBorder(color: R.color.gray06())
        
        nameLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        nameLabel.textColor = .black
        
        priceView.setCustomBorder(color: R.color.gray06())
        priceLabel.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        priceLabel.textColor = .black
        
        addToCartButton.setCustomBG(bgColor: UIColor.RestColors.orange)
        addToCartButton.setTitle("", for: .normal)
        addToCartButton.setImage(R.image.iconPlus(), for: .normal)
        addToCartButton.tintColor = .white
    }
}
