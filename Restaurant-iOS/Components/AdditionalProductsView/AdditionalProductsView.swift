//
//  AdditionalProductsView.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 05.12.2020.
//

import UIKit

protocol AdditionalProductsVMProtocol: ProductCellDelegate {
    var additionalProducts: [Product] { get set }
}


class AdditionalProductsView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            let layout = UICollectionViewFlowLayout()
            layout.estimatedItemSize = .zero
            layout.scrollDirection = .horizontal
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 8
            layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
            collectionView.collectionViewLayout = layout
            collectionView.alwaysBounceHorizontal = true
            collectionView.showsHorizontalScrollIndicator = false
            collectionView.dataSource = self
            collectionView.delegate = self
            registerCell()
        }
    }
    
    
    private var viewModel: AdditionalProductsVMProtocol!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    
    private func registerCell() {
        self.collectionView.register(R.nib.additionalProductCVCell)
    }
}


// MARK: - CollectionView DataSource
extension AdditionalProductsView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return viewModel.additionalProducts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.additionalProductCVCell, for: indexPath)!
        
        cell.setData(product: viewModel.additionalProducts[indexPath.item],
                     delegate: viewModel)
        return cell
    }
}



// MARK: - CollectionView Delegate
extension AdditionalProductsView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: collectionView.bounds.height)
    }
    
}



// MARK: - Assemble
extension AdditionalProductsView {
    static func assemble(title: String, viewModel: AdditionalProductsVMProtocol) -> AdditionalProductsView {
        let view = R.nib.additionalProductsView(owner: nil)!
        view.viewModel = viewModel
        view.titleLabel.text = title
        
        return view
    }
}
