//
//  CartBottomView.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 07.12.2020.
//

import UIKit
import RxSwift

protocol CartBottomViewDelegate {
    func showCartVC()
}


class CartBottomView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var fullPriceLabel: UILabel!
    @IBOutlet weak var chevronImageView: UIImageView!
    
    private let disposeBag = DisposeBag()
    private var delegate: CartBottomViewDelegate!
    
    
    private func setDesign() {
        self.backgroundColor = .white
        
        textLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        textLabel.textColor = .white
        
        fullPriceLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        fullPriceLabel.textColor = .white
        
        chevronImageView.tintColor = .white
        
        containerView.setCustomBG(bgColor: UIColor.RestColors.blue)
    }
    
    
    private func subscribe() {
        Cart.shared.fullPrice.subscribe(onNext: { [self] (fullPrice) in
            self.fullPriceLabel.text = "\(fullPrice) ₽"
            let bool = fullPrice > 0
            showBottomCartView(show: bool)
        }).disposed(by: disposeBag)
    }
    
    
    private func showBottomCartView(show: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: { () -> Void in
            if show {
                self.transform = CGAffineTransform(translationX: 0, y: -100)
            } else {
                self.transform = CGAffineTransform(translationX: 0, y: 0)
            }
        })
    }
    
    
    private func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                             action: #selector(goToCart))
        addGestureRecognizer(tapGesture)
    }
    
    
    @objc private func goToCart() {
        delegate?.showCartVC()
    }
}


// MARK: - Assemble
extension CartBottomView {
    
    static func assemble(text: String, delegate: CartBottomViewDelegate) -> CartBottomView {
        
        let view = R.nib.cartBottomView(owner: nil)!
        view.delegate = delegate
        view.textLabel.text = text
        view.setDesign()
        view.subscribe()
        view.addGesture()
        return view
    }
}
