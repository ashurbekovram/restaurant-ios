//
//  CartBottomViewPresentable.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 07.12.2020.
//

import UIKit

protocol CartBottomViewPresentable: class {
    func createCartBottomView(delegate: CartBottomViewDelegate)
}


extension UIViewController: CartBottomViewPresentable {
    
    func createCartBottomView(delegate: CartBottomViewDelegate) {
        let cartBottomView = CartBottomView.assemble(text: "Перейти к заказу",
                                                     delegate: delegate)
        view.addSubview(cartBottomView)
        
        
        let margins = view.layoutMarginsGuide
        
        cartBottomView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cartBottomView.topAnchor.constraint(equalTo: margins.bottomAnchor,
                                                constant: 34),
            cartBottomView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            cartBottomView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            cartBottomView.heightAnchor.constraint(equalToConstant: 100)
        ])
    }
    
}
