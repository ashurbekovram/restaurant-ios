//
//  ApiCart.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 03.01.2021.
//

import Foundation
import Alamofire


extension ApiManager {
    
    func getCart() -> DataRequest {
        return AF.request(url(with: "/order.json"))
    }
}
