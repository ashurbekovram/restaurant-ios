//
//  ApiTables.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 22.12.2020.
//

import Foundation
import Alamofire


extension ApiManager {
    
    func getTables() -> DataRequest {
        return AF.request(url(with: "/tableInfo.json"))
    }
}
