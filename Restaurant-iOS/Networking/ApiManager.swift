//
//  ApiManager.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import Foundation
import Alamofire

struct Api {
    static let v1 = ApiManager()
}


class ApiManager {
    // Вместо переменной session можно так же использовать стандартную переменную AF, которая так же является экземпляром класса Session()
    // fileprivate let session = Session()
    
    // Base URL
    private let baseURL = URL(string: "https://raw.githubusercontent.com/Aselder-Over/MockApiTestProject/main")!
    
    // Функция дополнения baseURL компонентами
    func url(with path: String, queryParams: [String : String] = [:]) -> URL {
        
        guard queryParams.count > 0 else { return baseURL.appendingPathComponent(path) }
        
        var urlComponent = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false)!
        
        urlComponent.queryItems = queryParams.map({URLQueryItem(name: $0, value: $1)})
        
        print(urlComponent)
        
        return try! urlComponent.asURL()
    }
    
}
