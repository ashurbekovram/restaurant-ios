//
//  ApiProduct.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 28.11.2020.
//

import Foundation
import Alamofire

extension ApiManager {
    func getProductData() -> DataRequest {
        return AF.request(url(with: "/productDetail.json"))
    }
}
