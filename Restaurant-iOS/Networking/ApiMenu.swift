//
//  ApiMenu.swift
//  Restaurant-iOS
//
//  Created by Рамазан on 15.11.2020.
//

import Foundation
import Alamofire

extension ApiManager {
    func getMenuData() -> DataRequest {
        return AF.request(url(with: "/apiMenu.json"))
    }
}
